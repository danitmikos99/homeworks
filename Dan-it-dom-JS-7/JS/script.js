"use strict";

const div = document.querySelector("#root");

function mainlist(arr, parent) {
  const arrLi = arr.map(function (item) {
    return `<li>${item}</li>`;
  });
  parent.insertAdjacentHTML("afterbegin", `<ul>${arrLi.join(" ")}</ul>`);
}
mainlist(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], div);

//Опишите своими словами, как Вы понимаете, что такое объектная модель документа (DOM)

// это структурированная последовательность объектов в документе, к которой к каждому элементу-ноды
// с легкостью можно добраться вызовом функции, которая указывает на тот или интой элемент.
